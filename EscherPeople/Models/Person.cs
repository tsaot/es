﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscherPeople.Models
{
    public class Person
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        public bool isMarried { get; set; }
        public bool? HasPermission { get; set; } = null;
        public Person Spouse { get; set; }

        public string SpouseFilePath { get; set; } = string.Empty; //default to empty string for path for fileoutput, probably not needed

        public string FullName { get => $"{FirstName} {Surname}"; }

        public string MarriedStatusString
        {
            get
            {
                return isMarried ? "Married" : "Single";
            }
        }

        public string FileOutput
        {
            get
            {
                return $"{FirstName}|{Surname}|{Birthdate:MM-dd-yyyy}|{MarriedStatusString}|{HasPermissionString}|{SpouseFilePath}";
            }
        }

        private string HasPermissionString
        {
            get => HasPermission.HasValue ? HasPermission.Value ? "yes" : "no" : "null"; 
        }

        public int Age
        {
            get
            {
                TimeSpan age = DateTime.Now.Subtract(Birthdate);
                DateTime ageInDateTime = DateTime.MinValue + age;
                return ageInDateTime.Year - DateTime.MinValue.Year;
            }
        }
    }
}
