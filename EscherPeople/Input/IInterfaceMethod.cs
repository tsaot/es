﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscherPeople.Input
{
    public interface IInterfaceMethod
    {
        /// <summary>
        /// Requests a string from the client
        /// </summary>
        /// <returns></returns>
        string requestStringInput();
        /// <summary>
        /// outputs a string to the client
        /// </summary>
        /// <param name="output"></param>
        void outputString(string output);
    }

    public class ConsoleInterface : IInterfaceMethod
    {
        public void outputString(string output)
        {
            Console.Write(Environment.NewLine + output + " ");
        }

        public string requestStringInput()
        {
            return Console.ReadLine();
        }
    }

    public class TestInterface : IInterfaceMethod
    {
        //Test app will load up the queue with the text
        public Queue<string> InputQueue { get; set; } = new Queue<string>();

        public void outputString(string output)
        {
            //do nothing here
        }

        public string requestStringInput()
        {
            return InputQueue.Dequeue();
        }
    }

    public class ClientInterfaceManager
    {
        private IInterfaceMethod _interfaceMethod;

        public ClientInterfaceManager(IInterfaceMethod interfaceMethod)
        {
            _interfaceMethod = interfaceMethod;
        }

        /// <summary>
        /// requests a non-whitespace string from the client
        /// </summary>
        /// <param name="promptName"></param>
        /// <returns></returns>
        public string requestStringInput(string promptName)
        {
            string result = null;
            while (string.IsNullOrWhiteSpace(result))
            {
                _interfaceMethod.outputString($"{promptName}:");
                result = _interfaceMethod.requestStringInput();
                if (string.IsNullOrWhiteSpace(result))
                {
                    _interfaceMethod.outputString($"{promptName} is required.");
                }
            }
            return result;
        }

        /// <summary>
        /// requests a formatted date input from the user and will re-prompt until it has it
        /// </summary>
        /// <param name="promptName"></param>
        /// <returns></returns>
        public DateTime requestDateInput(string promptName)
        {
            DateTime? result = null;
            while (result == null)
            {
                _interfaceMethod.outputString($"{promptName} in M/D/YYYY:");
                try
                {
                    result = DateParser(_interfaceMethod.requestStringInput());
                }
                catch (Exception ex)
                {
                    _interfaceMethod.outputString($"{promptName } not formatted correctly. Please retry.");
                }
            }
            return result.Value;
        }

        /// <summary>
        /// requests a yes/no answer from the user and will re-prompt until it has it
        /// </summary>
        /// <returns></returns>
        public bool requestYesNo()
        {
            var affirmatives = new List<string>() { "YES", "Y" };
            var negatives = new List<string>() { "NO", "N" };
            bool? result = null;
            while (!result.HasValue)
            {
                _interfaceMethod.outputString("Y/N:");
                string input = _interfaceMethod.requestStringInput();
                if (affirmatives.Contains(input.ToUpper())) //done this way to preserve the null flag
                { result = true; }
                else if (negatives.Contains(input.ToUpper()))
                { result = false; }
                else
                {
                    _interfaceMethod.outputString($"{input} is not a valid answer. Please input Y/N or Yes/No");
                }
            }
            return result.Value;
        }

        /// <summary>
        /// outputs a string to the user
        /// </summary>
        /// <param name="output"></param>
        public void OuputString(string output)
        {
            _interfaceMethod.outputString(output);
        }


        /// <summary>
        /// parses a string formatted as [month]/[day]/[4 digit year] 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static DateTime DateParser(string input)
        {
            return DateTime.ParseExact(input, "M/d/yyyy", CultureInfo.InvariantCulture);
        }
    }
}
