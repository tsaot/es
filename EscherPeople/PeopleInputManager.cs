﻿using EscherPeople.Input;
using EscherPeople.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscherPeople
{
    public class PeopleInputManager
    {
        private ClientInterfaceManager _interfaceManager;
        private string _personOutputFilePath;
        private string _spouseOutputFolder;

        private readonly int ParentalThreshold = 18; //todo: read these from app.config or from command line flags
        private readonly int ForbiddenThreshold = 16;

        public PeopleInputManager(IInterfaceMethod inputMethod, string personOutputFilePath, string spouseOuputFolder)
        {
            _interfaceManager = new ClientInterfaceManager(inputMethod);
            _personOutputFilePath = personOutputFilePath;
            _spouseOutputFolder = spouseOuputFolder;
        }

        /// <summary>
        /// Outputs welcome message and output paths to client
        /// </summary>
        public void RunIntro()
        {
            _interfaceManager.OuputString("Welcome to the Demo People Input Manager.");
            _interfaceManager.OuputString($"People output file: {_personOutputFilePath}");
            _interfaceManager.OuputString($"Spouse output folder: {_spouseOutputFolder}");
        }

        /// <summary>
        /// Main input loop - receives a person, saves it, and asks the user if they wish to add another
        /// </summary>
        public void RunInput()
        {
            bool acceptAnother = true;

            do
            {
                Person person = requestPerson();

                bool retrySave = false;

                try
                {
                    SavePerson(_personOutputFilePath, person);

                }
                catch (Exception ex)
                {
                    _interfaceManager.OuputString($"There was an error saving {person.FullName}. {ex.Message}");
                    _interfaceManager.OuputString("Try again?");
                    retrySave = _interfaceManager.requestYesNo();
                }

                _interfaceManager.OuputString("Add another person?");
                acceptAnother = _interfaceManager.requestYesNo();
            } while (acceptAnother);
        }

        /// <summary>
        /// Gets a person and possibly a spouse. Runs age check.
        /// </summary>
        /// <returns>Completed person data including spouse</returns>
        public Person requestPerson()
        {
            Person person = requestPersonInput();

            runAgeChecks(person);

            //continue input
            CheckSpouse(person);

            return person;
        }

        /// <summary>
        /// Gest basic person info
        /// </summary>
        /// <returns>Person with name and birthday filled in</returns>
        private Person requestPersonInput()
        {
            Person result = new Person()
            {
                FirstName = _interfaceManager.requestStringInput("First Name"),
                Surname = _interfaceManager.requestStringInput("Surame"),
                Birthdate = _interfaceManager.requestDateInput("Birthdate")
            };
            return result;
        }

        /// <summary>
        /// runs age checks on the passed in person
        /// </summary>
        /// <param name="person"></param>
        public void runAgeChecks(Person person)
        {
            if (person.Age < ForbiddenThreshold)
            {
                person.HasPermission = false;
            }
            else if (person.Age < ParentalThreshold)
            {
                _interfaceManager.OuputString($"{person.FullName} requires guardian consent to be resitered. Do they have consent from a legal guardian?");

                person.HasPermission = _interfaceManager.requestYesNo();
            }
        }

        /// <summary>
        /// Asks the user if they have a spouse and adds that spouse if needed
        /// </summary>
        /// <param name="person"></param>
        private void CheckSpouse(Person person)
        {
            _interfaceManager.OuputString($"Is {person.FullName} married?");
            person.isMarried = _interfaceManager.requestYesNo();
            if (person.isMarried)
            {
                person.Spouse = requestPersonInput();
                person.Spouse.isMarried = true;
            }
        }

        /// <summary>
        /// Saves a person or spouse to the passed in filepath, if there is a spouse attached, calls for the spouse to be saved first
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="person"></param>
        /// <param name="isSpouse"></param>
        private void SavePerson(string filePath, Person person, bool isSpouse = false)
        {
            if (!isSpouse && person.isMarried)
            {
                SaveSpouse(person);
            }
            try
            {
                File.AppendAllLines(filePath, new List<string> { person.FileOutput });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Generates the file path for a spouse, saves said path to the person object, and calls for the spouse to be saved.
        /// </summary>
        /// <param name="person"></param>
        private void SaveSpouse(Person person)
        {
            string spousePath = Path.Combine(_spouseOutputFolder, $"{DateTime.Now.Ticks}{person.Spouse.FirstName}.txt");
            person.SpouseFilePath = spousePath;
            if (!Directory.Exists(_spouseOutputFolder))
            {
                Directory.CreateDirectory(_spouseOutputFolder);
            }
            SavePerson(spousePath, person.Spouse, true);
        }
    }
}
