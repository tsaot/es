﻿using EscherPeople.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscherPeople
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string currentDir = Directory.GetCurrentDirectory();
            string mainOutputFile = Path.Combine(currentDir, "People.txt");
            string spouseOutputDir = Path.Combine(currentDir, "Spouses");


            PeopleInputManager peopleInputManager = new PeopleInputManager(new ConsoleInterface(), mainOutputFile, spouseOutputDir);
            peopleInputManager.RunIntro();
            peopleInputManager.RunInput();
        }
    }
}
