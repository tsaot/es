﻿using EscherPeople;
using EscherPeople.Input;
using EscherPeople.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace EscherPeopleTests
{
    [TestClass]
    public class UnitTest1
    {


        [TestMethod]
        public void TestValidInput()
        {
            TestInterface testInterface = new TestInterface();
            testInterface.InputQueue.Enqueue("Some");
            testInterface.InputQueue.Enqueue("Guy");
            testInterface.InputQueue.Enqueue("12/9/1988");
            testInterface.InputQueue.Enqueue("n");

            PeopleInputManager peopleInputManager = new PeopleInputManager(testInterface, "", "");
            Person person = peopleInputManager.requestPerson();
            Assert.IsNotNull(person);
            Assert.AreEqual(person.FullName, "Some Guy");
            Assert.AreEqual(person.FileOutput, "Some|Guy|12-09-1988|Single|null|");
        }

        [TestMethod]
        public void TestAgeCheckUnderage()
        {
            Person person = new Person()
            {
                Birthdate = DateTime.Now - TimeSpan.FromDays(365.25 * 10)
            };

            TestInterface testInterface = new TestInterface();
            PeopleInputManager peopleInputManager = new PeopleInputManager(testInterface, "", "");
            
            peopleInputManager.runAgeChecks(person);
            Assert.IsFalse(person.HasPermission);

        }

        [TestMethod]
        public void TestAgeCheckNeedsPermissionAndHasIt()
        {
            Person person = new Person()
            {
                Birthdate = DateTime.Now - TimeSpan.FromDays(365.25 * 17 + 10)
            };

            TestInterface testInterface = new TestInterface();
            testInterface.InputQueue.Enqueue("yes");
            PeopleInputManager peopleInputManager = new PeopleInputManager(testInterface, "", "");
            peopleInputManager.runAgeChecks(person);
            Assert.IsTrue(person.HasPermission);
        }

        [TestMethod]
        public void TestAgeCheckNeedsPermissionAndDoesNotHaveIt()
        {
            Person person = new Person()
            {
                Birthdate = DateTime.Now - TimeSpan.FromDays(365.25 * 17 + 10)
            };

            TestInterface testInterface = new TestInterface();
            testInterface.InputQueue.Enqueue("no");
            PeopleInputManager peopleInputManager = new PeopleInputManager(testInterface, "", "");
            peopleInputManager.runAgeChecks(person);
            Assert.IsFalse(person.HasPermission);
        }

        [TestMethod]
        public void TestAgeCheckOldEnough()
        {
            Person person = new Person()
            {
                Birthdate = DateTime.Now - TimeSpan.FromDays(365.25 * 42)
            };

            TestInterface testInterface = new TestInterface();
            PeopleInputManager peopleInputManager = new PeopleInputManager(testInterface, "", "");

            person.Birthdate = DateTime.Now - TimeSpan.FromDays(365.25 * 42);
            peopleInputManager.runAgeChecks(person);
            Assert.IsNull(person.HasPermission);
        }
    }
}
